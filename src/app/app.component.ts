import {ChangeDetectionStrategy, Component} from '@angular/core';
import {LocalStorageService} from "./core/services/local-storage/local-storage.service";
import {TranslateService} from "@ngx-translate/core";
import {LanguageStorageKey} from "./core/interfaces/translation";

@Component({
  selector: 'lm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  constructor(
    private readonly translateService: TranslateService,
    private readonly localStorageService: LocalStorageService,
  ) {
    const language = this.localStorageService.getItem(LanguageStorageKey) ?? this.translateService.defaultLang;
    this.translateService.use(language);
  }
}
