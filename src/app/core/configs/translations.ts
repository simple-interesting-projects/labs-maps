import {Language} from "../interfaces/translation";

export const TRANSLATIONS_MOCK: Record<Language, Record<string, string>> = {
  [Language.pl]: {
    'APP_TITLE': 'Lokalizacja Laboratorium',
    'LOAD_PL_TRANSLATION': 'Zaladuj translacji for PL',
    'LOAD_EN_TRANSLATION': 'Zaladuj translacji for EN',
    'ADDRESS_BAR': 'Pole adresu',
    'DOWNLOAD': 'Pobierz',
    'PLEASE_WAIT': 'Proszę czekać...',
    'FIELD_REQUIRED': 'Pole wymagane',
    'RESPONSE_ERROR': 'Błąd podczas próby pobrania danych',
    'NAME': 'Nazwa',
    'ADDRESS': 'Adres',
    'INFORMATION': 'Informacja',
  },
  [Language.en]: {
    'APP_TITLE': 'Laboratorium Location',
    'LOAD_PL_TRANSLATION': 'Load translation-loader for PL',
    'LOAD_EN_TRANSLATION': 'Load translation-loader for EN',
    'ADDRESS_BAR': 'Address bar',
    'DOWNLOAD': 'Download',
    'PLEASE_WAIT': 'Please wait...',
    'FIELD_REQUIRED': 'Field is required',
    'RESPONSE_ERROR': 'Error happens while try to load data',
    'NAME': 'Name',
    'ADDRESS': 'Address',
    'INFORMATION': 'Information',
  },
};
