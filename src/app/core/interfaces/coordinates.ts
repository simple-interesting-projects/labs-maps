export interface Coordinates {
  avg_lat: number;
  avg_lng: number;
  min_lng: number;
  min_lat: number;
  max_lng: number;
  max_lat: number;
  zoom: number;
}
