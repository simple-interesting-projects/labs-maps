export enum Language {
  pl = 'pl',
  en = 'en',
}

export const LanguageStorageKey = 'language-storage-key';
