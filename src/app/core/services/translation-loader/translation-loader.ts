import {Language} from "../../interfaces/translation";
import {delay, Observable, of} from "rxjs";
import {TranslateLoader} from "@ngx-translate/core";
import {TRANSLATIONS_MOCK} from "../../configs/translations";

export class TranslationLoader implements TranslateLoader {
  getTranslation(language: Language): Observable<Record<string, string>> {
    return of(TRANSLATIONS_MOCK[language]).pipe(delay(500));
  }
}
