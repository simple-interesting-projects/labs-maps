import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {LaboratoriumResponse} from "../../interfaces/laboratorium";

@Injectable({
  providedIn: 'root'
})
export class LaboratoriumService {

  constructor(private readonly httpClient: HttpClient) { }

  get(address: string) {
    return this.httpClient.get<LaboratoriumResponse>(address);
  }
}
