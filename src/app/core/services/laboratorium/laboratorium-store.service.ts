import {Injectable, OnDestroy} from '@angular/core';
import {Observable, ReplaySubject} from "rxjs";
import {Laboratorium, LaboratoriumResponse} from "../../interfaces/laboratorium";
import {Coordinates} from "../../interfaces/coordinates";

@Injectable({
  providedIn: 'root'
})
export class LaboratoriumStoreService implements OnDestroy {
  readonly labsResponse$: Observable<LaboratoriumResponse | undefined>;
  readonly coords$: Observable<Coordinates | undefined>;
  readonly labs$: Observable<Laboratorium[] | undefined>;
  private readonly labsResponseStream$: ReplaySubject<LaboratoriumResponse | undefined>;
  private readonly coordsStream$: ReplaySubject<Coordinates | undefined>;
  private readonly labsStream$: ReplaySubject<Laboratorium[] | undefined>;

  constructor() {
    this.labsResponseStream$ = new ReplaySubject<LaboratoriumResponse | undefined>(1);
    this.coordsStream$ = new ReplaySubject<Coordinates | undefined>(1);
    this.labsStream$ = new ReplaySubject<Laboratorium[] | undefined>(1);
    this.labsResponse$ = this.labsResponseStream$.asObservable();
    this.coords$ = this.coordsStream$.asObservable();
    this.labs$ = this.labsStream$.asObservable();
  }

  ngOnDestroy(): void {
    this.labsResponseStream$.complete();
    this.coordsStream$.complete();
    this.labsStream$.complete();
  }

  setLabsResponse(response?: LaboratoriumResponse): void {
    this.labsResponseStream$.next(response);
    this.coordsStream$.next(response?.cords ?? undefined);
    this.labsStream$.next(response?.laboatoria ?? undefined);
  }
}
