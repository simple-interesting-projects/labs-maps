import { TestBed } from '@angular/core/testing';

import { LaboratoriumStoreService } from './laboratorium-store.service';

describe('LaboratoriumStoreService', () => {
  let service: LaboratoriumStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LaboratoriumStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
