import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {LaboratoriumStoreService} from "../../core/services/laboratorium/laboratorium-store.service";
import {Subject, takeUntil} from "rxjs";
import {LaboratoriumResponse} from "../../core/interfaces/laboratorium";

@Component({
  selector: 'lm-json-preview',
  templateUrl: './json-preview.component.html',
  styleUrls: ['./json-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JsonPreviewComponent implements OnInit, OnDestroy {
  labsResponse?: LaboratoriumResponse;
  private readonly destroy$: Subject<void>;

  constructor(
    private readonly laboratoriumStoreService: LaboratoriumStoreService,
    private readonly changeDetectorRef: ChangeDetectorRef,
  ) {
    this.destroy$ = new Subject<void>();
  }

  ngOnInit() {
    this.laboratoriumStoreService.labsResponse$.pipe(takeUntil(this.destroy$)).subscribe((labsResponse) => {
      this.labsResponse = labsResponse;
      this.changeDetectorRef.detectChanges();
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
