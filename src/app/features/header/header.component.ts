import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Language, LanguageStorageKey} from "../../core/interfaces/translation";
import {LocalStorageService} from "../../core/services/local-storage/local-storage.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'lm-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {
  language = Language;

  constructor(
    private readonly translateService: TranslateService,
    private readonly localStorageService: LocalStorageService,
  ) { }

  load(language: Language): void {
    this.localStorageService.setItem(LanguageStorageKey, language);
    this.translateService.use(language);
  }
}
