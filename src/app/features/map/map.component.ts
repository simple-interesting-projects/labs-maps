import {AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import * as Leaflet from 'leaflet';
import {LaboratoriumStoreService} from "../../core/services/laboratorium/laboratorium-store.service";
import {Subject, takeUntil} from "rxjs";
import {Coordinates} from "../../core/interfaces/coordinates";
import {Laboratorium, LaboratoriumResponse} from "../../core/interfaces/laboratorium";
import {TranslateService} from "@ngx-translate/core";

Leaflet.Marker.prototype.options.icon = Leaflet.icon({
  iconRetinaUrl: 'assets/marker-icon-2x.png',
  iconUrl: 'assets/marker-icon.png',
  shadowUrl: 'assets/marker-shadow.png',
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});

@Component({
  selector: 'lm-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapComponent implements OnInit, AfterViewInit, OnDestroy {
  private map!: Leaflet.Map;
  private labsResponse?: LaboratoriumResponse;
  private readonly destroy$: Subject<void>;

  constructor(
    private readonly translateService: TranslateService,
    private readonly laboratoriumStoreService: LaboratoriumStoreService,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    this.destroy$ = new Subject<void>();
  }

  ngOnInit(): void {
    this.translateService.onLangChange.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.map.remove();
      this.initMap();
      if (this.labsResponse) {
        this.setCenter(this.labsResponse.cords);
        this.addRectangle(this.labsResponse.cords);
        this.addLabsMarkers(this.labsResponse.laboatoria);
      }
      this.changeDetectorRef.detectChanges();
    });
    this.laboratoriumStoreService.labsResponse$.pipe(takeUntil(this.destroy$)).subscribe((labsResponse) => {
      this.map.remove();
      this.initMap();
      if (labsResponse) {
        this.setCenter(labsResponse.cords);
        this.addRectangle(labsResponse.cords);
        this.addLabsMarkers(labsResponse.laboatoria);
      }
      this.labsResponse = labsResponse;
      this.changeDetectorRef.detectChanges();
    });
  }

  ngAfterViewInit(): void {
    this.initMap();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private initMap(): void {
    this.map = Leaflet.map('map', {
      center: [ 52.237049, 21.017532 ],
      zoom: 8
    });
    const tiles = Leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 3,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    tiles.addTo(this.map);
  }

  private addRectangle(coordinates: Coordinates): void {
    Leaflet.rectangle([
      [coordinates.min_lat, coordinates.min_lng],
      [coordinates.max_lat, coordinates.max_lng]
    ]).addTo(this.map);
  }

  private setCenter(coordinates: Coordinates): void {
    this.map.panTo(new Leaflet.LatLng(coordinates.avg_lat, coordinates.avg_lng));
    this.map.setZoom(coordinates.zoom);
  }

  private addLabsMarkers(labs: Laboratorium[]): void {
    labs.forEach((lab) => {
      const marker = Leaflet.marker([lab.gps_lat, lab.gps_lng]);
      const popupContent = `<p class="m-0">${this.translateService.instant('NAME')}: ${lab.nazwa}</p>
        <p class="m-0">${this.translateService.instant('ADDRESS')}: ${lab.adres}, ${lab.kod_pocztowy}, ${lab.miejscowosc}</p>
        ${lab.info ? '<p class="m-0">' + this.translateService.instant('INFORMATION') + ': ' + lab.info + '</p>' : ''}`;
      marker.bindPopup(popupContent);
      marker.addTo(this.map);
    });
  }
}
