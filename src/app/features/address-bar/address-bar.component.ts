import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {catchError, Subject, takeUntil, tap, throwError} from "rxjs";
import {LaboratoriumService} from "../../core/services/laboratorium/laboratorium.service";
import {LaboratoriumStoreService} from "../../core/services/laboratorium/laboratorium-store.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'lm-address-bar',
  templateUrl: './address-bar.component.html',
  styleUrls: ['./address-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddressBarComponent implements OnDestroy {
  form: FormGroup;
  isLoading: boolean;
  private readonly destroy$: Subject<void>;

  constructor(
    private readonly snackBar: MatSnackBar,
    private readonly formBuilder: FormBuilder,
    private readonly translateService: TranslateService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly laboratoriumService: LaboratoriumService,
    private readonly laboratoriumStoreService: LaboratoriumStoreService,
  ) {
    this.isLoading = false;
    this.form = this.formBuilder.group({
      address: ['', Validators.required],
    });
    this.destroy$ = new Subject<void>();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  submit(): void {
    if (this.form.valid) {
      this.isLoading = true;
      (this.form.get('address') as AbstractControl).disable();
      this.laboratoriumService
        .get((this.form.get('address') as AbstractControl).value)
        .pipe(
          tap((response) => {
            if (!response.laboatoria || !response.cords) {
              throw new Error(this.translateService.instant('RESPONSE_ERROR'));
            }
          }),
          catchError((error) => {
            this.laboratoriumStoreService.setLabsResponse();
            this.snackBar.open(this.translateService.instant('RESPONSE_ERROR'));
            this.isLoading = false;
            (this.form.get('address') as AbstractControl).enable();
            this.changeDetectorRef.detectChanges();
            return throwError(error);
          }),
          takeUntil(this.destroy$),
        ).subscribe((response) => {
          this.laboratoriumStoreService.setLabsResponse(response);
          this.isLoading = false;
          (this.form.get('address') as AbstractControl).enable();
          this.changeDetectorRef.detectChanges();
        });
    }
  }
}
